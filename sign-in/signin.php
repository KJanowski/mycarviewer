<?php session_start();?>
<?php if(isset($_SESSION['LoggedIn'])){
    header('Location:../dashboard\dashboard.php'); 
    die();
}?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>


    <!-- Bootstrap core CSS -->
<link href="../assets/dist/css/bootstrap.min.css" rel="stylesheet">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>

    
    <!-- Custom styles for this template -->
    <link href="signin.css" rel="stylesheet">
  </head>
  <body class="text-center bg-dark" >
    
<main class="form-signin">
      <!-- ---------------------------------------FORM START-------------------------------------------- -->
  <form id="LoginForm" autocomplete="off">
    <img class="mb-2" src="../logo_wh.png" height="107">
    <div class="form-message"><?php if(isset($_GET['registered'])){
       echo "<p style='background-color:#32CD32;'> zarejestrowano </p>";

}  ?></div>

    <div class="form-floating">
      <input type="text" name="username" class="form-control" id="floatingInput" >
      <label for="floatingInput">Podaj nazwę użytkownika</label>
    </div>

    <div class="form-floating">
      <input type="password" name="password" class="form-control" id="floatingPassword" placeholder="Password">
      <label for="floatingPassword">Podaj hasło</label>
    </div>

    <div class="checkbox mb-3 text-white">
      <label>
        <input type="checkbox" value="remember-me"> Zapamiętaj mnie
      </label>
    </div>
    
    <!-- <a class="btn w-100 btn btn-lg btn-secondary" href="../dashboard/dashboard.html" role="button">Zaloguj</a> -->
    <input class="btn w-100 btn btn-lg btn-secondary" href="../dashboard/dashboard.html" type="submit" name="submit_button" value="Zaloguj">

    <br><br><br><br>

    <p class="text-white">Nie masz konta?</p>
    <a class="btn w-100 btn btn-lg btn-secondary" href="../sign-up/signup.php" role="button">Zarejestruj się</a>

  </form>
      <!-- ---------------------------------------/FORM -------------------------------------------- -->

</main>

      <!-- ---------------------------------------/AJAX -------------------------------------------- -->
      <script type="text/javascript">
        $(document).ready(function(){

            $("#LoginForm").on('submit', function(e){
              e.preventDefault();
              $.ajax({
                type: "POST",
                url: "signin_conditions.php",
                data: new FormData(this),
                dataType: "json",
                contentType: false,
                cache: false,
                processData: false,

                success:function(response){
                  $(".form-message").css("display","block");

                  if(response.status == 1){
                    $("#LoginForm")[0].reset(); 
                    $(".form-message").html('<p style="background-color:#32CD32;">' + response.message + '</p>');
                    window.location.href = 'https://mycarviewer.infinityfreeapp.com/dashboard/dashboard.php';

                  }else{

                    $(".form-message").html('<p style="background-color:tomato;">' + response.message + '</p>');
                  }
                }
              });
            });
            // $("#file").change(function(){
            //   var file = this.files[0];
            // });
            });


      </script>


      <!-- ---------------------------------------/AJAX -------------------------------------------- -->

  </body>
</html>
