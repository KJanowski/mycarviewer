/* globals Chart:false, feather:false */

(function () {
  'use strict'

  feather.replace({ 'aria-hidden': 'true' })

  // Graphs
  var ctx = document.getElementById('myChart')
  // eslint-disable-next-line no-unused-vars
  var myChart = new Chart(ctx, {
    type: 'line',
    data: {
      labels: [
        'Grudzień',
        'Styczeń',
        'Luty',
        'Marzec',
        'Kwiecień',
        'Maj'
      ],
      datasets: [{
        data: [
          89,
          156,
          241,
          192,
          72,
          98
        ],
        lineTension: 0,
        backgroundColor: 'transparent',
        borderColor: '#007bff',
        borderWidth: 4,
        pointBackgroundColor: '#007bff'
      }]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: false
          },
        }]
      },
      legend: {
        display: false
      }
    }
  })
})()
