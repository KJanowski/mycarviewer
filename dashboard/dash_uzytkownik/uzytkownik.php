<?php session_start();?>
<?php  include '../../includes/functions.php' ?>


<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">


  <!-- Bootstrap core CSS -->
  <link href="../../assets/dist/css/bootstrap.min.css" rel="stylesheet">

  <style>
    .bd-placeholder-img {
      font-size: 1.125rem;
      text-anchor: middle;
      -webkit-user-select: none;
      -moz-user-select: none;
      user-select: none;
    }

    @media (min-width: 768px) {
      .bd-placeholder-img-lg {
        font-size: 3.5rem;
      }
    }
  </style>


  <!-- Custom styles for this template -->
  <link href="dashboard.css" rel="stylesheet">
</head>

<body>

  <header class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
    <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="#">My Car Viewer</a>
    <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse"
      data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search">
    <div class="navbar-nav">
      <div class="nav-item text-nowrap">
        <a class="nav-link px-3" href="#">Sign out</a>
      </div>
    </div>
  </header>

  <div class="container-fluid">
    <div class="row">
      <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
        <div class="position-sticky pt-3">
          <ul class="nav flex-column">
            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="../../dashboard/dashboard.php">
                <span data-feather="home"></span>
                Panel główny
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../dash_auta/auta.html">
                <span data-feather="truck"></span>
                Pojazdy
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
                <span data-feather="users"></span>
                Użytkownik
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
                <span data-feather="bar-chart-2"></span>
                Reports
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
                <span data-feather="layers"></span>
                Integrations
              </a>
            </li>
          </ul>

          <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span>Saved reports</span>
            <a class="link-secondary" href="#" aria-label="Add a new report">
              <span data-feather="plus-circle"></span>
            </a>
          </h6>
          <ul class="nav flex-column mb-2">
            <li class="nav-item">
              <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Current month
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Last quarter
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Social engagement
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Year-end sale
              </a>
            </li>
          </ul>
        </div>
      </nav>

      <div class="row justify-content-start">
        <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
          <div class="">
            <h1 class="h2"></br>Panel użytkownika</h1>
            <br><br>


    <div class="card border-primary w-50 mb-3" ">
          <div class=" card-header">Sekcja Kilometrów</div>
            <div class="card-body text-primary row justify-content-around ">
              <div class="card  col-sm-7">
                <div class="card-body">
                  W tym miesiącu przejechałeś następującą liczbę kilometrów:
                </div>
              </div>
              <div class="card col-sm-2">
                <div class="card-body">
                  XXX
                </div>
              </div>
            </div>
            <div class="card-body text-primary row justify-content-around ">
              <div class="card  col-sm-7">
                <div class="card-body">
                  W tym miesiącu przejechałeś następującą liczbę kilometrów:
                </div>
              </div>
              <div class="card col-sm-2">
                <div class="card-body">
                  XXX
                </div>
              </div>
            </div>
            <div class="card-body text-primary row justify-content-around">
              <div class="card col-sm-7">
                <div class="card-body">
                  Obecny przebieg auta:
                </div>
              </div>
              <div class="card col-sm-2">
                <div class="card-body">
                  XXX
                </div>
              </div>

              <?php 
                if(isset($_POST['dodaj_km'])){
                  (int) $dodaj = $_POST['fname'];
                  $username = $_SESSION['username'];
                  $array = ["Styczen", "Luty", "Marzec", "Kwiecien", "Maj", "Czerwiec", "Lipiec", "Sierpien", "Wrzesien", "Pazdziernik", "Listopad", "Grudzien"];
                  $data = date('m') ;



                  $query_a  = "SELECT * FROM mileage WHERE username = '$username' ";
                  $result_km = mysqli_query($connection, $query_a);
                  $rows = [];
                  while($row = mysqli_fetch_assoc($result_km))
                  {
                      $rows[] = $row['value'];
                  
                  }
                  $date = date('m');
                  $date = $date[1] - 1 ;
                  $date1 = date('m');
                  (int)$ostat = $rows [12 - $date1];
                  // print("ttutaj sie wyswietli:");
                  // print($date);
                  // print($ostat);
              //    print($date);
                // + $dodaj
                $dodaj = $ostat + $dodaj ;
                  $query_km = "UPDATE mileage SET value = $dodaj WHERE username = '$username' AND month_id = $date  ";

                  //$query_km = "UPDATE mileage SET value = $dodaj WHERE  month_id = $aktualny_miesiac  ";
                // $query_km = "UPDATE mileage SET value = $data WHERE username = '$username' ";
                  $result = mysqli_query($connection, $query_km);
                }

        ?> 
              <p class="text-white">1</p>
                <p class="text-dark">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nowe kilomery</p>

                <form action="" method="POST">
                <div class="card-body text-primary row justify-content-around">
                  <div class="form-group col-4">
                    <p >Wybierz miesiąc:</p>
                    <select class="form-control" id="miesiac">
                      <option>Styczeń</option>
                      <option>Luty</option>
                      <option>Marzec</option>
                      <option>Kwiecień</option>
                      <option>Maj</option>
                      <option>Czerwiec</option>
                      <option>Lipiec</option>
                      <option>Sierpień</option>
                      <option>Wrzesień</option>
                      <option>Październik</option>
                      <option>Listopad</option>
                      <option>Grudzień</option>
                    </select>
                  </div>
                  <div class="form-group col-sm-4">
                    <p >Podaj liczbę kilometrów:</p>
                    <input type="number" class="form-control" id="liczba" placeholder="km" name="fname">
                  </div>
                  <div class="col-sm-2">
                    <p class="text-white">1</p>
                    <button type="submit" class="btn btn-primary" name="dodaj_km">Dodaj</button>
                  </div>     
              </form>
            </div>
            </div>
          </div>
          <div class="card border-danger w-50 mb-3" ">
            <div class=" card-header">Sekcja Paliwa</div>
              <div class="card-body text-danger row justify-content-around ">
                <div class="card  col-sm-7">
                  <div class="card-body">
                    W tym miesiącu spaliłeś następującą liczbę litrów paliwa:
                  </div>
                </div>
                <div class="card col-sm-2">
                  <div class="card-body">
                    XXX
                  </div>
                </div>
              </div>
              <div class="card-body text-danger row justify-content-around ">
                <div class="card  col-sm-7">
                  <div class="card-body">
                    W tym roku spaliłeś następującą liczbę litrów paliwa:
                  </div>
                </div>
                <div class="card col-sm-2">
                  <div class="card-body">
                    XXX
                  </div>
                </div>
              </div>
  
                
                <p class="text-white">1</p>
                  <p class="text-dark">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Zatankowane paliwo</p>
  
                  <form>
                  <div class="card-body text-danger row justify-content-around">
                    <div class="form-group col-4">
                      <p >Wybierz miesiąc:</p>
                      <select class="form-control" id="miesiac_dwa">
                        <option>Styczeń</option>
                        <option>Luty</option>
                        <option>Marzec</option>
                        <option>Kwiecień</option>
                        <option>Maj</option>
                        <option>Czerwiec</option>
                        <option>Lipiec</option>
                        <option>Sierpień</option>
                        <option>Wrzesień</option>
                        <option>Październik</option>
                        <option>Listopad</option>
                        <option>Grudzień</option>
                      </select>
                    </div>
                    <div class="form-group col-sm-4">
                      <p >Podaj liczbę zatankowanego paliwa:</p>
                      <input type="number" class="form-control" id="paliwo" placeholder="l">
                    </div>
                    <div class="col-sm-2">
                      <p class="text-white">1</p>
                      <button type="submit" class="btn btn-danger">Dodaj</button>
                    </div>     
                </form>
              </div>
              </div>
            </div>

            <div class="card border-success w-50 mb-3" ">
              <div class=" card-header">Sekcja Mechanika</div>
                <div class="card-body text-success row justify-content-around ">
                  <p class="text-dark">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ostatnie naprawy:</p>
                  <table class="table table-Default">
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Typ</th>
                        <th scope="col">Koszt</th>
                        <th scope="col">Mechanik</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row">1</th>
                        <td>Przebita opona</td>
                        <td>600 zł</td>
                        <td>Adam Adamowski</td>
                      </tr>
                      <tr>
                        <th scope="row">2</th>
                        <td>Awaria sprzęgła</td>
                        <td>430 zł</td>
                        <td>Tomasz Zbagieński</td>
                      </tr>
                      <tr>
                        <th scope="row">3</th>
                        <td>Wymiana klocków hamulcowych</td>
                        <td>180 zł</td>
                        <td> Adam Adamowski</td>
                      </tr>
                    </tbody>
                  </table>
                  
                  <p class="text-white">1</p>
                    <p class="text-dark">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Zlecenie naprawy</p>
    
                    <form>
                      <div class="card-body text-success row justify-content-around">
                        <div class="form-group col-4">
                          <p >Element do zdiagnozowania:</p>
                          <select class="form-control" id="miesiac">
                            <option>Układ elektryczny</option>
                            <option>Sprzęgło</option>
                            <option>Koła</option>
                            <option>Silnik</option>
                            <option>Hamulce</option>
                            <option>Czerwiec</option>
                            <option>Lipiec</option>
                            <option>Sierpień</option>
                            <option>Wrzesień</option>
                            <option>Październik</option>
                            <option>Listopad</option>
                            <option>Grudzień</option>
                          </select>
                        </div>
                        <div class="form-group col-sm-4">
                          <p >Wybierz dostępnego mechanika:</p>
                          <select class="form-control" aria-label="Default select example" id="miesiac">
                            <option selected>Adam Adamowski</option>
                            <option>Stanisław Brama</option>
                            <option>Tomasz Dobrogowski</option>
                            <option>Filip Pająk</option>
                            <option>Dominik Szwed</option>
                            <option>Tomasz Zbagieński</option>
                          </select>
                        </div>
                        <div class="col-sm-2">
                          <p class="text-white">1</p>
                          <button type="submit" class="btn btn-success">Zleć</button>
                        </div>     
                    </form>
                </div>
                </div>
              </div>



        </main>
      </div>
    </div>
    <script src="../../assets/dist/js/bootstrap.bundle.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/feather-icons@4.28.0/dist/feather.min.js"
      integrity="sha384-uO3SXW5IuS1ZpFPKugNNWqTZRRglnUJK6UAZ/gxOX80nxEkN9NcGZTftn6RzhGWE"
      crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js"
      integrity="sha384-zNy6FEbO50N+Cg5wap8IKA4M/ZnLJgzc6w2NqACZaK0u0FXfOWRRJOnQtpZun8ha"
      crossorigin="anonymous"></script>
    <script src="dashboard.js"></script>

</body>

</html>