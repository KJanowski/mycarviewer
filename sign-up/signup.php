<?php session_start();?>
<?php if(isset($_SESSION['LoggedIn'])){
    header('Location:../dashboard\dashboard.php'); 
    die();
}?>
<?php  include '../includes\database_connection.php' ?>
<?php  include '../includes\functions.php' ?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <!-- Bootstrap core CSS -->
<link href="../assets/dist/css/bootstrap.min.css" rel="stylesheet">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>

    
    <!-- Custom styles for this template -->
    <link href="signup.css" rel="stylesheet">
  </head>
  <body class="text-center bg-dark">
    
<main class="form-signin">

<!-- -----------------------form start------------------------ -->
  <form id="RegistrationForm" autocomplete="off">
    <img class="mb-2" src="../logo_wh.png" height="107">
   
      <div class="form-message" ></div>

    <div class="form-group">
      <label for="sel1">Wybierz rolę konta:</label>
      <select class="form-control" id="sel1" name="Account_type">
        <option value="jeden">Właściciel pojazdu</option>
        <option value="dwa">Mechanik</option>
        <option value="trzy">Ubezpieczyciel</option>
      </select>
    </div>
    
    <br>
    <div class="form-floating">
      <input autocomplete="false" type="text" name="username" class="form-control" id="floatingInput" >
      <label for="floatingInput">Podaj nazwę użytkownika</label>
    </div>

    <div class="form-floating">
      <input autocomplete="false" type="password" name="password" class="form-control" id="floatingPassword" placeholder="Password">
      <label for="floatingPassword">Podaj hasło</label>
    </div>

    <div class="form-floating">
      <input  autocomplete="false" type="password" name="rpassword" class="form-control" id="floatingRpassword" placeholder="Rpassword">
      <label for="floatingPassword">Potwierdź hasło</label>
    </div>

    <!-- <a class="btn w-100 btn btn-lg btn-secondary" href="../dashboard/dashboard.html" role="button"> Stwórz konto</a> -->
    <input class="btn w-100 btn btn-lg btn-secondary" href="../dashboard/dashboard.html" type="submit" name="submit_button" value="Stwórz konto">
  </form>
  
  <!-- ----------------------------------------------- -->

</main>

    <script type="text/javascript">
        $(document).ready(function(){

          $("#RegistrationForm").on('submit', function(e){
            e.preventDefault();
            $.ajax({
              type: "POST",
              url: "signup_conditions.php",
              data: new FormData(this),
              dataType: "json",
              contentType: false,
              cache: false,
              processData: false,

              success:function(response){
                $(".form-message").css("display","block");

                if(response.status == 1){
                  $("#RegistrationForm")[0].reset(); 
                  // $(".form-message").html('<p style="background-color:tomato;">' + response.message + '</p>');
                  window.location.href = 'https://mycarviewer.infinityfreeapp.com/sign-in/signin.php?registered=yes';

                }else{

                  $(".form-message").html('<p style="background-color:tomato;">' + response.message + '</p>');
                }
              }
            });
          });
          // $("#file").change(function(){
          //   var file = this.files[0];
          // });
        });
    </script>
    




  </body>
</html>
