<?php session_start();?>
<?php  include '../includes/database_connection.php'; ?>
<?php  include '../includes/functions.php'; ?>
<?php 

// function username_exists($username){
//   global $connection;

//   $query = "SELECT username FROM users WHERE username = '{$username}'";
//   $result = mysqli_query($connection, $query);
//  // confirmQuery($result);

//   if(mysqli_num_rows($result) > 0) {

//       return true;

//   } else {

//       return false;

//   }
//   return true;

// }

      $response = array(
        'status' => 0,
        'message' => 'Form submission Failed',
      );

      $errorEmpty = false;
      $errorEmail = false;

      //Warunek sprawdza, czy formularz został wysłany 
      if(isset($_POST['username']) || isset($_POST['password']) || isset($_POST['rpassword']) ){
        $username =  $_POST['username'];
        $password =  $_POST['password'];
        $rpassword = $_POST['rpassword'];
        $account_type = $_POST['account_type'];

        if(!empty($username) && !empty($password) && !empty($rpassword)){

          if(username_exists($username) == true){
          //  if(1 == 1){
           $response['message'] = "Nazwa użytkownika zajęta";
          }else if(strlen($username) < 4 || strlen($password) < 4){
            $response['message'] = "Za krótkie hasło lub nazwa";
          }else if(strlen($username) > 15 || strlen($password) > 15){
            $response['message'] = "Za długie hasło lub nazwa";
          }else if($password != $rpassword){
            $response['message'] = "Źle powtórzone hasło";
          }else{
           // $response['message'] = "tutaj jest problem";
            $response['message'] = "Konto zostanie założone";


            Registration($username, $password, $account_type);
            $response['status'] = 1;

          }

        }else{
          $response['message'] = "jedno z pól jest puste";
        }

      }
      echo json_encode($response);

    ?>